
from flask import Flask, request
import json
import os
from pathlib import Path
import sys

app = Flask(__name__)

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'model.py')
import model
#
# The base64 encoded input and style image are received
# from the Node.js server and are being fed into the
# style transfer model. After the training process the model
# returns the result image and an array containing images from
# from previous epochs.
#
@app.route("/styleTransfer", methods=['GET', 'POST'])
def received():
    if (request.is_json):
        json_images = request.get_json()
        content = json_images['input']
        style = json_images['style']
        evolutionStep = 50 # determins the amount of evolutions
        styledImgB64, imageEvolutions = model.style_transfer_main(
            content, style, evolutionStep)
    return {"image": styledImgB64 , "list": json.dumps(imageEvolutions) }


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080')