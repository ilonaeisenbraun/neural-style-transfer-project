# Neural Style Transfer Projekt
This is our Neural Style Project, which has been created in the course of the module "Ausgewählte Kapitel sozialer Webtechnologien".


Our application is based on Node.JS, Flask and Python. The reachable service can be found here: http://34.90.216.211:8888/ .


The implementation is based on Neural Style Transfer by [Leon Gatys](https://github.com/leongatys/PytorchNeuralStyleTransfer). 
Hereby, we are working with a novel Neural Network, similarly, implementing VGG19.


***


## Setup & Installation

Currently we are supporting two methods in order to run our application locally:

### - Containerized Setup
* *requires Docker being installed*
* for bigger iterations, it is advised to use CUDA to serve the workload on GPU
* Navigate to our Container Registry [here](https://gitlab.com/ilonaeisenbraun/neural-style-transfer-project/container_registry).
___
* Pull the images separately, both App and Server (including the Model):
    * **docker pull registry.gitlab.com/ilonaeisenbraun/neural-style-transfer-project:app**
    * **docker pull registry.gitlab.com/ilonaeisenbraun/neural-style-transfer-project:server_100** (here you can specify the Docker image, tagged with the number of iterations)
____
* Run the App and the Server image concurrently:

    * **docker run --net=host registry.gitlab.com/ilonaeisenbraun/neural-style-transfer-project:app**
    * **docker run --net=host registry.gitlab.com/ilonaeisenbraun/neural-style-transfer-project:server_100**
___  
* the host mapping is used to enable cross-container communication. Hereby, you can specify another IP rather than localhost, eg.:
    * **docker run --net=host --ip=127.0.0.1 registry.gitlab.com/ilonaeisenbraun/neural-style-transfer-project:server_100**

### - Manual Setup
* requires *Node, NPM, Python 3.6*
___
* **git pull** our repository

* navigate to *app*:
    * run **npm install** to install all required packages from *package.json*
    * run **npm start** to start the web application

___
* navigate to *server*:
    * run **pip install -r server-requirements.txt** (preferably from inside of a virtual environment)
    * run the script: **python server/app.py**
___
* You can now style Images locally!
